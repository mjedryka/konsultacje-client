import {
  createSwitchNavigator,
  createStackNavigator,
} from 'react-navigation'

import LoadingScreen from '../src/Screen/Auth/LoadingScreen'
import SignInScreen from '../src/Screen/Auth/SignInScreen'

import ProfileScreen from '../src/Screen/ProfileScreen'

import HostScreen from '../src/Screen/Host/HostScreen'
import AddTimeWindowScreen from '../src/Screen/Host/AddTimeWindowScreen'

import GuestScreen from '../src/Screen/Guest/GuestScreen'
import AddTimeScreen from '../src/Screen/Guest/AddTimeScreen'
import TimeWindowsScreen from '../src/Screen/Guest/TimeWindowsScreen'


const AppStack = createStackNavigator({
  Profile: ProfileScreen,

  Host: HostScreen,
  AddTimeWindow: AddTimeWindowScreen,

  Guest: GuestScreen,
  AddTime: AddTimeScreen,
  TimeWindows: TimeWindowsScreen,
})
const AuthStack = createStackNavigator({
  SignIn: SignInScreen,
})

export default createSwitchNavigator(
  {
    AuthLoading: LoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
)
