import moment from 'moment'


export default class User {
  /**
   * @public
   * @param {object} data
   */
  constructor(data) {
    if (!data) {
      this.brokenInputData = true
      return
    }
    this._id = data.phone || data._id
    this.name = data.name
    this.surname = data.surname
    this.uuid = data.uuid
    this.lastActive = data.lastActive ? moment(data.lastActive).toDate() : null
    this.session = data.session
    this.profile = data.profile
  }

  /**
   * @public
   * @returns {string}
   */
  get phone() {
    return this._id
  }

  /**
   * @public
   * @returns {boolean}
   */
  get isValid() {
    return !this.brokenInputData
  }
}
