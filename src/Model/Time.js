import moment from 'moment'


export default class Time {
  /**
   * @public
   * @param {object} data
   * @param {string} hostId
   */
  constructor(data, hostId) {
    if (!data) {
      this.brokenInputData = true
      return
    }
    this.guestId = data.guestId.toString()
    this.startTime = moment(data.startTime, moment.ISO_8601).toDate()
    this.endTime = moment(data.endTime, moment.ISO_8601).toDate()
    if (hostId) {
      this.hostId = hostId
    }
  }

  /**
   * @public
   * @returns {boolean}
   */
  get isValid() {
    return !this.brokenInputData
  }

  /**
   * @public
   * @param {Time} time
   * @returns {boolean}
   */
  overlapWith(time) {
    let m = moment(this.startTime)
    if (m.isBetween(time.startTime, time.endTime) || m.isSame(time.startTime)) {
      return true
    }

    m = moment(this.endTime);

    return m.isBetween(time.startTime, time.endTime) || m.isSame(time.endTime)
  }
}
