import moment from 'moment'

import Time from './Time'


export default class TimeWindow {
  /**
   * @public
   * @param {object} data
   */
  constructor(data) {
    if (!data) {
      this.brokenInputData = true
      return
    }
    if (data._id) {
      this._id = data._id
    }
    this.hostId = data.hostId.toString()
    this.startTime = moment(data.startTime, moment.ISO_8601).toDate()
    this.endTime = moment(data.endTime, moment.ISO_8601).toDate()
    this.times = data.times.map(t => new Time(t)) || []
  }

  /**
   * @public
   * @returns {boolean}
   */
  get isValid() {
    return !this.brokenInputData
  }

  /**
   * @public
   * @returns {boolean}
   */
  get hasFreeTime() {
    /**
     * @private
     * @param {Date} startTime
     * @param {number} durationMinutes
     * @returns {Time}
     */
    const createTime = (startTime, durationMinutes = 10) =>
      new Time({
        guestId: '',
        startTime,
        endTime: moment(startTime).add(durationMinutes, 'minute').toDate(),
      }, this.hostId)

    let offset = 0
    let time = new Time(null, '')

    do {
      time = createTime(moment(this.startTime).add(offset, 'minute').toDate())
      if (this.isFree(time)) {
        return true
      }
      offset++
    } while (time.endTime < this.endTime)

    return false
  }

  /**
   * @public
   * @param {Time} time
   * @returns {boolean}
   */
  isFree(time) {
    if (!moment(this.startTime).isSameOrBefore(time.startTime)) {
      return false
    }
    if (!moment(this.endTime).isSameOrAfter(time.endTime)) {
      return false
    }

    return this.times.filter(t => t.overlapWith(time)).length === 0
  }
}
