import {
  StyleSheet,
} from 'react-native'


export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    height: 50,
    maxHeight: 50,
  },
  picker: {
    width: 160,
  },
})

export const pickerCustom = {
  dateIcon: {
    position: 'absolute',
    left: 0,
    top: 4,
    marginLeft: 0,
  },
  dateInput: {
    marginLeft: 36,
  },
}
