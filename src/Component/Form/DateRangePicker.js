import React from 'react'
import PropTypes from 'prop-types'
import {
  View,
} from 'react-native'
import DatePicker from 'react-native-datepicker'

import moment from 'moment'

import styles, {
  pickerCustom,
} from './DateRangePicker.style'


const DateRangePicker = props => (
  <View style={styles.container}>
    <DatePicker
      style={styles.picker}
      date={props.start}
      mode={props.mode}
      placeholder="Data początkowa"
      format={props.format}
      minDate={props.min}
      maxDate={props.max}
      confirmBtnText="Potwierdź"
      cancelBtnText="Anuluj"
      customStyles={pickerCustom}
      onDateChange={date => props.onChanged(moment(date).toDate(), props.end)}
    />
    <DatePicker
      style={styles.picker}
      date={props.end}
      mode={props.mode}
      placeholder="Data końcowa"
      format={props.format}
      minDate={props.min}
      maxDate={props.max}
      confirmBtnText="Potwierdź"
      cancelBtnText="Anuluj"
      customStyles={pickerCustom}
      onDateChange={date => props.onChanged(props.start, moment(date).toDate())}
    />
  </View>
)

DateRangePicker.propTypes = {
  start: PropTypes.instanceOf(Date).isRequired,
  end: PropTypes.instanceOf(Date).isRequired,
  onChanged: PropTypes.func.isRequired,
  min: PropTypes.instanceOf(Date),
  max: PropTypes.instanceOf(Date),
  mode: PropTypes.oneOf('date', 'datetime', 'time'),
  format: PropTypes.string,
}

DateRangePicker.defaultProps = {
  min: moment('2018-01-01').toDate(),
  max: moment('2030-12-31').toDate(),
  mode: 'date',
  format: 'YYYY-MM-DD',
}

export default DateRangePicker
