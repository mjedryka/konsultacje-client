import React from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native'


const TextInputBox = props => (
  <View style={styles.container}>
    <Text>{props.label}</Text>
    <TextInput
      keyboardType={props.type}
      placeholder={props.label}
      style={styles.input}
      value={props.value}
      onChangeText={props.onChangeText}
    />
  </View>
)

TextInputBox.propTypes = {
  label: PropTypes.string.isRequired,
  type: PropTypes.oneOf('default', 'numeric'),
  value: PropTypes.string.isRequired,
  onChangeText: PropTypes.func.isRequired,
}

export default TextInputBox


const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ddd',
    padding: 10,
    margin: 10,
  },
  input: {
    marginTop: 10,
    width: 200,
  },
})
