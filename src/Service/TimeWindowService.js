import TimeWindow from '../Model/TimeWindow'

import ApiService from './ApiService'
import UserService from './UserService'


export default class TimeWindowService {
  /**
   * @public
   * @param {string} hostPhone
   * @param {Date} startTime
   * @param {Date} endTime
   * @return {Promise.<TimeWindow[]>}
   */
  static async getHostsTimeWindows(hostPhone, startTime, endTime) {
    const response = await ApiService.pull('GET', path(`host/${hostPhone}`), {
      startTime: startTime.toISOString(),
      endTime: endTime.toISOString(),
    })
    if (!response.ok) {
      return null
    }

    const result = []
    for (const entry of await response.json()) {
      result.push(new TimeWindow(entry))
    }
    return result
  }

  static async createTimeWindow(hostPhone, startTime, endTime) {
    const user = await UserService.currentUser()
    if (!user.isValid || user.phone !== hostPhone) {
      return null
    }

    const response = await ApiService.send('PUT', path(''), {
      hostId: hostPhone,
      startTime: startTime.toISOString(),
      endTime: endTime.toISOString(),
      uuid: user.uuid,
      session: user.session,
    })
    if (!response.ok) {
      return null
    }

    return await response.json()
  }
}

/**
 * @private
 * @param {string} endpoint
 * @returns {string}
 */
const path = endpoint => `/timeWindow/${endpoint}`
