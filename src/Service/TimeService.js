import Time from '../Model/Time'

import ApiService from './ApiService'
import UserService from './UserService'


export default class TimeService {
  /**
   * @public
   * @returns {Promise.<Time[]>}
   */
  static async times() {
    const user = await UserService.currentUser()
    if (!user.isValid) {
      return []
    }

    const response = await ApiService.pull('GET', path(''), {
      phone: user.phone,
    })
    if (!response.ok) {
      return []
    }

    const result = []
    for (const entry of await response.json()) {
      console.log('entry', entry)
      result.push(new Time(entry, entry.hostId))
    }
    return result
  }

  /**
   * @public
   * @param {string} windowId
   * @param {Date} startTime
   * @param {Date} endTime
   * @returns {Promise.<void>}
   */
  static async add(windowId, startTime, endTime) {
    const user = await UserService.currentUser()
    if (!user.isValid) {
      return null
    }

    const response = await ApiService.send('PUT', path(''), {
      session: user.session,
      uuid: user.uuid,
      guestId: user.phone,
      windowId,
      startTime,
      endTime,
    })
    if (!response.ok) {
      return null
    }

    return await response.json()
  }
}

/**
 * @private
 * @param {string} endpoint
 * @returns {string}
 */
const path = endpoint => `/time/${endpoint}`
