import {
  AsyncStorage,
} from 'react-native'

import User from '../Model/User'

import ApiService from './ApiService'


export default class UserService {
  /**
   * @public
   * @return {Promise.<void>}
   */
  static async signOut() {
    return await AsyncStorage.setItem('user', '')
  }

  /**
   * @public
   * @returns {Promise.<User>}
   */
  static async currentUser() {
    return new User(JSON.parse(await AsyncStorage.getItem('user')))
  }

  /**
   * @public
   * @return {Promise.<User[]>}
   */
  static async hostUsers() {
    const user = await UserService.currentUser()
    if (!user.isValid) {
      return []
    }

    const response = await ApiService.pull('GET', path(`${user.phone}/hosts`), null)
    if (!response.ok) {
      return []
    }

    const result = []
    for (const entry of await response.json()) {
      console.log('host user entry', entry)
      result.push(new User(entry))
    }
    return result
  }

  /**
   * @public
   * @param {string} phone
   * @param {string} name
   * @param {string} surname
   * @param {string} uuid
   * @returns {Promise.<User>}
   */
  static async createUser({phone, name, surname, uuid}) {
    const response = await ApiService.send('PUT', path(phone), {
      name,
      surname,
      uuid,
    })
    if (!response.ok) {
      return null
    }
    return new User(await response.json())
  }

  /**
   * @public
   * @param {string} profile
   * @returns {Promise.<User>}
   */
  static async setProfile(profile) {
    const user = new User(JSON.parse(await AsyncStorage.getItem('user')))
    if (!user.isValid) {
      return null
    }
    const response = await ApiService.send('PATCH', path(`${user.phone}/profile`), {
      profile,
      uuid: user.uuid,
      session: user.session,
    })
    if (!response.ok) {
      return null
    }
    user.profile = (await response.json()).profile
    return user
  }

  /**
   * @public
   * @returns {Promise.<User>}
   */
  static async promoteSession() {
    const user = new User(JSON.parse(await AsyncStorage.getItem('user')))
    if (!user.isValid) {
      return null
    }
    const response = await ApiService.send('PATCH', path(`${user.phone}/session`), {
      uuid: user.uuid,
      session: user.session,
    })
    if (!response.ok) {
      return null
    }
    user.session = (await response.json()).session
    return user
  }
}

/**
 * @private
 * @param {string} endpoint
 * @returns {string}
 */
const path = endpoint => `/user/${endpoint}`
