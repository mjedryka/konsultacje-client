const API_URL = 'http://192.168.1.5:6545'

export default class ApiService {
  /**
   * @public
   * @param {string} method
   * @param {string} path
   * @param {object} body
   * @returns {Promise.<Response>}
   */
  static async send(method, path, body) {
    const url = path.startsWith('/') ? `${API_URL}${path}` : `${API_URL}/${path}`
    console.log(`sending ${method} to ${url}`)
    console.log(body)
    return fetch(url, {
      method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    })
  }

  /**
   * @public
   * @param {string} method
   * @param {string} path
   * @param {object} query
   * @returns {Promise.<Response>}
   */
  static async pull(method, path, query) {
    const url = path.startsWith('/') ? `${API_URL}${path}` : `${API_URL}/${path}`
    console.log(`sending ${method} to ${parametrize(url, query)}`)
    console.log(query)
    return fetch(parametrize(url, query), {
      method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }
}

/**
 * @private
 * @param {string} url
 * @param {object} query
 * @return {string}
 */
const parametrize = (url, query) => {
  let result = url
  for (const key in query) {
    if (!query.hasOwnProperty(key)) {
      continue
    }
    result += result.contains('?') ? '&' : '?'
    result += `${key}=${query[key]}`
  }
  return result
}
