import DeviceInfo from 'react-native-device-info'


export default class DeviceService {
  /**
   * @public
   * @returns {string}
   */
  static get phoneNumber() {
    return DeviceInfo.getPhoneNumber() || ''
  }

  /**
   * @public
   * @returns {string}
   */
  static get uniqueId() {
    return DeviceInfo.getUniqueID()
  }
}
