import React from 'react'
import {
  AsyncStorage,
  Button,
  StyleSheet,
  View,
} from 'react-native'

import autobind from 'autobind-decorator'

import DeviceService from '../../Service/DeviceService'
import UserService from '../../Service/UserService'

import TextInputBox from '../../Component/Form/TextInputBox'


export default class SignInScreen extends React.Component {
  /**
   * @protected
   * @param {object} props
   */
  constructor(props) {
    super(props)
    this.state = {
      phoneNumber: DeviceService.phoneNumber,
      name: '',
      surname: '',
      uuid: DeviceService.uniqueId,
    }
  }

  /**
   * @private
   * @returns {Promise.<void>}
   */
  @autobind
  async onSave() {
    const {
      phoneNumber,
      name,
      surname,
      uuid,
    } = this.state
    const user = await UserService.createUser({
      phone: phoneNumber,
      name,
      surname,
      uuid,
    })

    if (!user.isValid) {
      return Promise.reject('Got invalid user object!')
    }
    try {
      await AsyncStorage.setItem('user', JSON.stringify(user))
    } catch (e) {
      console.error('Storing user error', e)
      return Promise.reject('Storing user on the device failed.')
    }

    this.props.navigation.navigate('Profile')
  }

  /**
   * @protected
   * @returns {Component}
   */
  render() {
    return (
      <View style={styles.container}>
        <TextInputBox
          label={'Numer telefonu'}
          type={'numeric'}
          value={this.state.phoneNumber}
          onChangeText={text => this.setState({ phoneNumber: text })}
        />
        <TextInputBox
          label={'Imię'}
          type={'default'}
          value={this.state.name}
          onChangeText={text => this.setState({ name: text })}
        />
        <TextInputBox
          label={'Nazwisko'}
          type={'default'}
          value={this.state.surname}
          onChangeText={text => this.setState({ surname: text })}
        />
        <Button
          title={'Zapisz'}
          onPress={this.onSave}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    width: 200,
  },
})

