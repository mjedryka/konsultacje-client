import React from 'react'
import {
  StyleSheet,
  Text,
  View,
} from 'react-native'

import UserService from '../../Service/UserService'


export default class LoadingScreen extends React.Component {
  async componentWillMount() {
    const user = await UserService.currentUser()
    console.log('user at bootstrap', user)
    this.props.navigation.navigate(user.isValid ? 'App' : 'Auth')
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Ładowanie aplikacji...</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
})
