import {
  StyleSheet,
} from 'react-native'


export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  profileContainer: {
    marginTop: 20,
    width: 200,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-evenly',
  },
})
