import React from 'react'
import {
  Button,
  FlatList,
  Text,
  View,
} from 'react-native'

import moment from 'moment'
import 'moment/locale/pl'

import autobind from 'autobind-decorator'

import DateRangePicker from '../../Component/Form/DateRangePicker'

import TimeWindowService from '../../Service/TimeWindowService'
import UserService from '../../Service/UserService'

import styles from './HostScreen.style'


export default class HostScreen extends React.Component {
  static navigationOptions = {
    title: 'Gospodarz',
  }

  /**
   * @protected
   * @type {{startDate: Date, endDate: Date, timeWindowArray: TimeWindow[]}}
   */
  state = {
    startDate: moment().startOf('day').toDate(),
    endDate: moment().endOf('day').add(1, 'month').toDate(),
    timeWindowArray: [],
    isLoading: false,
  }

  /**
   * @protected
   * @returns {Promise.<void>}
   */
  componentWillMount() {
    moment.locale('pl')
    this.loadTimeWindows().then()
  }

  /**
   * @private
   * @returns {Promise.<void>}
   */
  @autobind
  async loadTimeWindows() {
    this.setState({
      isLoading: true,
    })

    const user = await UserService.currentUser()
    const { startDate, endDate } = this.state

    this.setState({
      timeWindowArray: await TimeWindowService.getHostsTimeWindows(user.phone, startDate, endDate),
      isLoading: false,
    })
  }

  /**
   * @private
   * @param {Date} startDate
   * @param {Date} endDate
   * @returns {void}
   */
  @autobind
  onDateRangeChanged(startDate, endDate) {
    this.setState({
      startDate,
      endDate,
    })
    this.loadTimeWindows().then()
  }

  /**
   * @private
   * @param {object} item
   * @returns {XML}
   */
  renderTimeWindow({ item }) {
    return (
      <View style={styles.timeWindowContainer}>
        <View style={styles.dateContainer}>
          <Text>{moment(item.startTime).format('LLLL')}</Text>
          <Text>{moment(item.endTime).format('LLLL')}</Text>
        </View>
        <Text style={styles.guestCounter}>{item.times.length || 0}</Text>
      </View>
    )
  }

  /**
   * @protected
   * @returns {XML}
   */
  render() {
    const {
      timeWindowArray,
    } = this.state

    return (
      <View style={styles.container}>
        <DateRangePicker
          start={this.state.startDate}
          end={this.state.endDate}
          onChanged={this.onDateRangeChanged}
        />
        <Text>Liczba okien czasowych: {timeWindowArray.length}</Text>
        <FlatList
          data={timeWindowArray}
          keyExtractor={item => item._id}
          renderItem={this.renderTimeWindow}
        />
        <Button
          title='Dodaj okno czasowe'
          onPress={() => this.props.navigation.push('AddTimeWindow')}
        />
      </View>
    )
  }
}
