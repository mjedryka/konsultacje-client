import {
  Dimensions,
  StyleSheet,
} from 'react-native'


const {
  width,
} = Dimensions.get('window')

export default StyleSheet.create({
  container: {
    paddingTop: 3,
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  timeWindowContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderColor: '#ded',
    borderTopWidth: 1,
    width: width * 0.85,
    height: 52,
    paddingVertical: 6,
  },
  dateContainer: {
    flex: 5,
    flexDirection: 'column',
  },
  guestCounter: {
    width: 40,
    height: 40,
    backgroundColor: '#f62',
    borderRadius: 20,
    color: '#fff',
    fontSize: 20,
    paddingVertical: 5,
    paddingLeft: 14,
  },
})
