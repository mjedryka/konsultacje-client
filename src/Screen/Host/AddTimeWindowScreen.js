import React from 'react'
import {
  Button,
  StyleSheet,
  View,
} from 'react-native'

import autobind from 'autobind-decorator'
import moment from 'moment'

import DateRangePicker from '../../Component/Form/DateRangePicker'

import TimeWindowService from '../../Service/TimeWindowService'
import UserService from '../../Service/UserService'


export default class GuestScreen extends React.Component {
  static navigationOptions = {
    title: 'Dodaj okno czasowe',
  }
  /**
   * @protected
   * @type {{startDate: Date, endDate: Date}}
   */
  state = {
    startDate: moment().startOf('hour').add(1, 'hour').toDate(),
    endDate: moment().startOf('hour').add(2, 'hour').toDate(),
  }


  /**
   * @private
   * @param {Date} startDate
   * @param {Date} endDate
   * @returns {void}
   */
  @autobind
  onDateRangeChanged(startDate, endDate) {
    this.setState({
      startDate,
      endDate,
    })
  }

  @autobind
  async createTimeWindow() {
    console.log('createTimeWindow')
    const user = await UserService.currentUser()
    const {
      startDate,
      endDate,
    } = this.state
    console.log(user.phone, startDate, endDate)
    if (await TimeWindowService.createTimeWindow(user.phone, startDate, endDate)) {
      this.props.navigation.goBack()
    }
  }

  /**
   * @protected
   * @returns {XML}
   */
  render() {
    return (
      <View style={styles.container}>
        <DateRangePicker
          start={this.state.startDate}
          end={this.state.endDate}
          onChanged={this.onDateRangeChanged}
        />
        <Button
          title='Utwórz'
          onPress={this.createTimeWindow}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 3,
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
})
