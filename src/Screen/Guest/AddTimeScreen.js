import React from 'react'
import {
  Button,
  View,
} from 'react-native'

import autobind from 'autobind-decorator'

import moment from 'moment'

import DateRangePicker from '../../Component/Form/DateRangePicker'

import TimeService from '../../Service/TimeService'

import styles from './AddTimeScreen.style'


export default class AddTimeScreen extends React.Component {
  static navigationOptions = {
    title: 'Dodaj termin',
  }

  /**
   * @private
   * @type {string}
   */
  format = 'YYYY-MM-DD HH:mm'

  /**
   * @protected
   * @type {{startDate: Date, endDate: Date}}
   */
  state = {
    startDate: new Date(),
    endDate: new Date(),
  }

  /**
   * @public
   * @returns {string}
   */
  get windowId() {
    const window = this.props.navigation.getParam('window')

    return window ? window._id : ''
  }

  componentWillMount() {
    const window = this.props.navigation.getParam('window')
    console.log('window', window)
    console.log(typeof window)

    this.minDate = moment(window.startTime).toDate()
    this.maxDate = moment(window.endTime).toDate()

    console.log('min date', this.minDate)
    console.log('max date', this.maxDate)

    this.setState({
      startDate: this.minDate,
      endDate: this.maxDate,
    })
  }

  /**
   * @private
   * @param {Date} startDate
   * @param {Date} endDate
   * @returns {void}
   */
  @autobind
  onDateRangeChanged(startDate, endDate) {
    this.setState({
      startDate,
      endDate,
    })
  }

  /**
   * @private
   * @returns {void}
   */
  @autobind
  async createTime() {
    const { startDate, endDate } = this.state

    if (await TimeService.add(this.windowId, startDate, endDate)) {
      this.props.navigation.navigate('Guest', {
        tab: 'timeList',
      })
    }
  }

  /**
   * @protected
   * @returns {React.Component}
   */
  render() {
    return (
      <View style={styles.container}>
        <DateRangePicker
          start={this.state.startDate}
          end={this.state.endDate}
          onChanged={this.onDateRangeChanged}
          mode='datetime'
          format={this.format}
          min={this.minDate}
          max={this.maxDate}
        />
        <Button
          title='Utwórz'
          onPress={this.createTime}
        />
      </View>
    )
  }
}
