import React from 'react'
import {
  Button,
  FlatList,
  Text,
  TouchableHighlight,
  View,
} from 'react-native'

import autobind from 'autobind-decorator'

import moment from 'moment'
import 'moment/locale/pl'

import TimeService from '../../Service/TimeService'
import UserService from '../../Service/UserService'

import styles from './GuestScreen.style'


export default class GuestScreen extends React.Component {
  static navigationOptions = {
    title: 'Gość',
  }

  /**
   * @protected
   * @type {{hostUserArray: User[], timeArray: Time[]}}
   */
  state = {
    tab: '',
    hostUserArray: [],
    timeArray: [],
  }

  /**
   * @private
   * @returns {string}
   */
  get tabName() {
    return this.props.navigation.getParam('tab', 'hostUsers')
  }

  /**
   * @private
   * @param {string} [tab]
   * @returns {string}
   */
  title(tab = this.tabName) {
    switch (tab) {
      case 'hostUsers':
        return 'Wybierz gospodarza'
      case 'timeList':
        return 'Twoje terminy'
      default:
        return ''
    }
  }

  /**
   * @protected
   * @returns {void}
   */
  componentWillMount() {
    this.setState({
      tab: this.tabName,
    })
    this.fetchData().then()
  }

  /**
   * @private
   * @return {Promise.<void>}
   */
  @autobind
  async fetchData() {
    switch (this.tabName) {
      case 'hostUsers':
        this.setState({
          hostUserArray: await UserService.hostUsers()
        })
        break
      case 'timeList':
        this.setState({
          timeArray: await TimeService.times()
        })
        break
    }
  }

  /**
   * @private
   * @param {string} phone
   * @returns {void}
   */
  @autobind
  selectHost(phone) {
    this.props.navigation.push('TimeWindows', { phone })
  }

  /**
   * @private
   * @param {string} tab
   * @returns {void}
   */
  @autobind
  changeTab(tab) {
    this.props.navigation.setParams({
      tab,
    })
    this.setState({
      tab,
    })
    this.fetchData().then()
  }

  /**
   * @private
   * @param {User} item
   * @returns {React.Component}
   */
  @autobind
  renderHostUser({ item }) {
    return (
      <View style={styles.userContainer}>
        <TouchableHighlight
          style={styles.userBox}
          activeOpacity={0.7}
          onPress={() => this.selectHost(item.phone)}
        >
          <View style={styles.userData}>
            <Text>{item.name} {item.surname}</Text>
            <Text>Tel. {item.phone}</Text>
          </View>
        </TouchableHighlight>
      </View>
    )
  }

  /**
   * @private
   * @param {Time} item
   * @returns {React.Component}
   */
  @autobind
  renderTime({ item }) {
    return (
      <Text style={styles.time}>
        {moment(item.startTime).format('LLLL')} do {moment(item.endTime).format('LLLL')}
      </Text>
    )
  }

  /**
   * @protected
   * @returns {React.Component}
   */
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{this.title()}</Text>
        {{
          'hostUsers': (
            <FlatList
              data={this.state.hostUserArray}
              keyExtractor={item => item._id}
              renderItem={this.renderHostUser}
            />
          ),
          'timeList': (
            <FlatList
              data={this.state.timeArray}
              keyExtractor={item => `${item.startTime} - ${item.endTime}`}
              renderItem={this.renderTime}
              inverted
            />
          ),
        }[this.tabName]}
        <View style={styles.tabs}>
          <Button
            title={this.title('hostUsers')}
            onPress={() => this.changeTab('hostUsers')}
          />
          <Button
            title={this.title('timeList')}
            onPress={() => this.changeTab('timeList')}
          />
        </View>
      </View>
    )
  }
}