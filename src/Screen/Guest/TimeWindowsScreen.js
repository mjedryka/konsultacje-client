import React from 'react'
import {
  Button,
  FlatList,
  Text,
  TouchableHighlight,
  View,
} from 'react-native'

import moment from 'moment'
import 'moment/locale/pl'

import autobind from 'autobind-decorator'

import DateRangePicker from '../../Component/Form/DateRangePicker'

import TimeWindowService from '../../Service/TimeWindowService'

import styles from './TimeWindowsScreen.style'


export default class TimeWindowsScreen extends React.Component {
  static navigationOptions = {
    title: 'Okna czasowe gospodarza',
  }

  /**
   * @protected
   * @type {{startDate: Date, endDate: Date, timeWindowArray: TimeWindow[]}}
   */
  state = {
    startDate: moment().startOf('day').toDate(),
    endDate: moment().endOf('day').add(1, 'month').toDate(),
    timeWindowArray: [],
  }

  /**
   * @protected
   * @returns {Promise.<void>}
   */
  componentWillMount() {
    this.loadTimeWindows().then()
  }

  /**
   * @private
   * @returns {Promise.<void>}
   */
  @autobind
  async loadTimeWindows() {
    const phone = this.props.navigation.getParam('phone')
    if (!phone) {
      return Promise.reject('No host phone specified!')
    }

    const { startDate, endDate } = this.state

    this.setState({
      timeWindowArray: await TimeWindowService.getHostsTimeWindows(phone, startDate, endDate),
    })
  }

  /**
   * @private
   * @param {Date} startDate
   * @param {Date} endDate
   * @returns {void}
   */
  @autobind
  onDateRangeChanged(startDate, endDate) {
    this.setState({
      startDate,
      endDate,
    })
    this.loadTimeWindows().then()
  }

  /**
   * @private
   * @param {TimeWindow} window
   * @returns {void}
   */
  @autobind
  addTime(window) {
    this.props.navigation.push('AddTime', {
      window,
    })
  }

  /**
   * @private
   * @param {TimeWindow} item
   * @returns {React.Component}
   */
  @autobind
  renderTimeWindow({ item }) {
    return (
      <View style={styles.timeWindowContainer}>
        <View style={styles.dateContainer}>
          <Text>{moment(item.startTime).format('LLLL')}</Text>
          <Text>{moment(item.endTime).format('LLLL')}</Text>
        </View>
        { item.hasFreeTime
        && (
          <TouchableHighlight
            onPress={() => this.addTime(item)}
          >
            <Text style={styles.add}>+</Text>
          </TouchableHighlight>
        )
        || (
          <Text>brak miejsc</Text>
        )}
      </View>
    )
  }

  /**
   * @protected
   * @returns {React.Component}
   */
  render() {
    const {
      timeWindowArray,
    } = this.state

    return (
      <View style={styles.container}>
        <DateRangePicker
          start={this.state.startDate}
          end={this.state.endDate}
          onChanged={this.onDateRangeChanged}
        />
        <Text>Liczba okien czasowych: {timeWindowArray.length}</Text>
        <FlatList
          data={timeWindowArray}
          keyExtractor={item => item._id}
          renderItem={this.renderTimeWindow}
        />
      </View>
    )
  }
}
