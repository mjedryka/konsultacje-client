import {
  StyleSheet,
} from 'react-native'


export default StyleSheet.create({
  container: {
    paddingTop: 3,
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
})
