import {
  Dimensions,
  StyleSheet,
} from 'react-native'


const {
  width,
} = Dimensions.get('window')

export default StyleSheet.create({
  container: {
    paddingTop: 3,
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  timeWindowContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderColor: '#ded',
    borderTopWidth: 1,
    width: width * 0.85,
    height: 52,
    paddingVertical: 6,
  },
  dateContainer: {
    flex: 5,
    flexDirection: 'column',
  },
  add: {
    width: 40,
    height: 34,
    backgroundColor: '#3cff70',
    borderRadius: 10,
    color: '#fff',
    fontSize: 18,
    paddingVertical: 4,
    paddingLeft: 15,
  },
})
