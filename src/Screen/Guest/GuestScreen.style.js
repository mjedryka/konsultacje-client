import {
  Dimensions,
  StyleSheet,
} from 'react-native'


const {
  width,
} = Dimensions.get('window')

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  userContainer: {
    backgroundColor: '#333',
    borderColor: '#111',
    borderWidth: 2,
    borderRadius: 20,
    padding: 8,
    margin: 4,
  },
  userBox: {
    backgroundColor: '#ddd',
    borderRadius: 17,
  },
  userData: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    width: width * 0.6,
    height: 80,
  },
  tabs: {
    flex:1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    height: 35,
    maxHeight: 35,
    width: width * 0.95,
  },
})
