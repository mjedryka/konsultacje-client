import React from 'react'
import {
  AsyncStorage,
  Button,
  StyleSheet,
  View,
} from 'react-native'

import autobind from 'autobind-decorator'

import UserService from '../Service/UserService'

import styles from './ProfileScreen.style'


export default class ProfilePage extends React.Component {
  /**
   * @protected
   * @param {object} props
   */
  constructor(props) {
    super(props)
    this.state = {
      profile: ''
    }
  }

  /**
   * @private
   * @param {string} role
   * @returns {Promise.<void>}
   */
  @autobind
  async changeRole(role) {
    if (!['Host', 'Guest'].includes(role)) {
      return Promise.reject(`Unsupported role ${role}`)
    }

    const user = await UserService.setProfile(role.toLowerCase())
    await AsyncStorage.setItem('user', JSON.stringify(user))

    this.props.navigation.push(role)
  }

  /**
   * @private
   * @returns {Promise.<void>}
   */
  @autobind
  async signOut() {
    await UserService.signOut()
    this.props.navigation.popToTop()
  }

  /**
   * @protected
   * @returns {Component}
   */
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.profileContainer}>
          <Button
            title={'Gość'}
            onPress={() => this.changeRole('Guest')}
          />
          <Button
            title={'Gospodarz'}
            onPress={() => this.changeRole('Host')}
          />
        </View>
        <Button
          title={'Wyloguj się'}
          color={'red'}
          onPress={this.signOut}
        />
      </View>
    )
  }
}
