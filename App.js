import React from 'react'
import {
  AsyncStorage,
  YellowBox,
} from 'react-native';

import Navigation from './config/Navigation'

import UserService from './src/Service/UserService'


YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated'])

export default class App extends React.Component {
  /**
   * @protected
   * @returns {void}
   */
  componentDidMount() {
    this.sessionIntervalId = setInterval(async () => {
      console.log('promoting session...')
      const user = await UserService.promoteSession()
      if (user && user.isValid) {
        await AsyncStorage.setItem('user', JSON.stringify(user))
        console.log('updated user by new session')
      }
    }, 50 * 1000)
  }

  /**
   * @protected
   * @returns {void}
   */
  componentWillUnmount() {
    clearInterval(this.sessionIntervalId)
  }

  /**
   * @protected
   * @returns {XML}
   */
  render() {
    return (
      <Navigation />
    )
  }
}
